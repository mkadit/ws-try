FROM golang:1.21-alpine3.19 AS builder
WORKDIR /app
COPY . .
RUN go build -mod=vendor -o main ./cmd/ws-try/main.go

FROM golang:1.21-alpine3.19 AS builder-dev
WORKDIR /app
COPY . .
RUN go build -mod=vendor -o helper ./cmd/ws-try-helper/main.go
RUN ./common/script/install.sh

# RUN go build -o main ./cmd/microsite-backend/main.go
# Run stage
FROM alpine:3.19 AS prod
WORKDIR /app
RUN apk --no-cache add tzdata
COPY --from=builder /app/internal/db/migrations/ ./internal/db/migrations/
COPY --from=builder /app/main .

EXPOSE 8081
CMD [ "/app/main" ]

FROM alpine:3.19 AS cli
WORKDIR /app
RUN apk --no-cache add tzdata
COPY --from=builder /app/internal/db/migrations/ ./internal/db/migrations/
COPY --from=builder /app/helper .

EXPOSE 8081
ENTRYPOINT [ "/app/helper" ]

FROM golang:1.21-bullseye as dev
WORKDIR /app
COPY --from=builder-dev /go/bin /go/bin
CMD [ "make", "watch" ] 

