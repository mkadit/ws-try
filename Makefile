#!make

current_dir = $(dir $(abspath $(firstword $(MAKEFILE_LIST))))
ifneq (,$(wildcard ./app.env))
    include app.env
    export
endif

up:
	mkdir -p data
	mkdir -p logs
	mkdir -p mongo-data
	docker compose --env-file app.env --profile prod up -d

dev:
	mkdir -p data
	mkdir -p logs
	mkdir -p mongo-data
	docker compose --env-file app.env --profile dev up -d

up_db:
	mkdir -p data
	mkdir -p logs
	mkdir -p mongo-data
	docker compose --env-file app.env up -d db
	docker compose --env-file app.env up -d cache

up_clean:
	docker compose --env-file app.env up --build -d

clean:
	docker container prune

down:
	docker compose --profile dev --profile prod down

down_server:
	docker compose --env-file app.env down server

down_db:
	docker compose --env-file app.env down db
	docker compose --env-file app.env down cache
	docker compose --env-file app.env down server

sql_gen:
	sqlc -f ./common/config/sqlc.yaml generate

docs_gen:
	swag init -g ./cmd/ws-try/main.go

test:
	go test -cover -v ./... -coverprofile=coverage.out

make-migration:
	migrate create -ext sql -dir internal/db/migrations -seq $(filter-out $@,$(MAKECMDGOALS))

migrate:
	migrate -database "postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable" -path "internal/db/migrations" up

migrate-down:
	migrate -database "postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable" -path "internal/db/migrations" drop -f

migrate-reset:
	migrate -database "postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable" -path "internal/db/migrations" drop -f
	migrate -database "postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable" -path "internal/db/migrations" up

watch:
	air -c ./common/config/.air.toml

generate:
	# docker compose --env-file app.env up server-generator
	go run ./common/script/dataGenerator.go

test_simple:
	docker compose --env-file app.env up server-tester

play:
	go run ./cmd/playground/main.go

build:
	# docker build -t gitlab.artajasa.co.id:5050/itd/ade/microsite-msb/backend-microsite-msb --target prod .
	# docker build --file Dockerfile.generator -t gitlab.artajasa.co.id:5050/itd/ade/microsite-msb/specification:generate-data .

prod_build:
	docker compose --env-file app.env --profile prod build 

dev_build:
	docker compose --env-file app.env --profile dev build 

helper_build:
	docker compose --env-file app.env --profile helper build 

prod_push:
	docker compose --env-file app.env --profile prod push

dev_push:
	docker compose --env-file app.env --profile dev push

helper_push:
	docker compose --env-file app.env --profile helper push
