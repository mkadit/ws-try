/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package generate

import (
	"context"
	"fmt"
	"gitlab.com/mkadit/ws-try/cmd/ws-try-helper/cmd/util"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// GenerateCmd represents the generate command
var GenerateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate test data for ws-try",
	Long:  `Generate test data for ws-try`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		env, _ := cmd.PersistentFlags().GetString("env")
		viper.Set("ENV", env)
		s := util.ConnectStack()
		ctx := context.WithValue(cmd.Context(), "server", s)
		cmd.SetContext(ctx)
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("generate called")
		cmd.Help()
	},
}

func addSubCommandsPalettes() {
	GenerateCmd.AddCommand(UserCmd)
}

func init() {
	addSubCommandsPalettes()
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
