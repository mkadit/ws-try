/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package generate

import (
	"fmt"
	"sync"
	"gitlab.com/mkadit/ws-try/cmd/ws-try-helper/cmd/util"
	"gitlab.com/mkadit/ws-try/internal/api"

	"github.com/spf13/cobra"
)

// UserCmd represents the user command
var UserCmd = &cobra.Command{
	Use:   "user",
	Short: "Generate user test data",
	Long:  `Generate user test data`,
	Run: func(cmd *cobra.Command, args []string) {
		num, _ := cmd.Flags().GetInt("many")
		var wg sync.WaitGroup
		ctx := cmd.Context()

		s := ctx.Value("server").(api.Server)
		wg.Add(1)
		go func() {
			defer wg.Done()
			util.GenerateUser(&wg, s.DB, ctx, num)
		}()

		wg.Wait()
		fmt.Printf("Users generated: %d\n", num)
	},
}

func init() {
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// userCmd.PersistentFlags().String("foo", "", "A help for foo")

	UserCmd.Flags().IntP("many", "n", 10, "Number of generated data")
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// userCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
