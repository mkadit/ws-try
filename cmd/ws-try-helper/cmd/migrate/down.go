/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package migrate

import (
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/spf13/cobra"
)

// downCmd represents the down command
var DownCmd = &cobra.Command{
	Use:   "down",
	Short: "Migrate down",
	Long:  `Migrate down`,
	Run: func(cmd *cobra.Command, args []string) {
		steps, _ := cmd.Flags().GetInt("steps")
		ctx := cmd.Context()

		m := ctx.Value("migration").(*migrate.Migrate)
		m.Steps(-1 * steps)
		fmt.Printf("%d steps migrate downs\n", steps)
	},
}

func init() {
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// downCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// downCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
