/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package migrate

import (
	"context"
	"gitlab.com/mkadit/ws-try/cmd/ws-try-helper/cmd/util"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// migrateCmd represents the migrate command
var MigrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Migrate app database",
	Long:  `Migrate app database`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		env, _ := cmd.PersistentFlags().GetString("env")
		viper.Set("ENV", env)
		m, _ := util.ConnectMigration()
		ctx := context.WithValue(cmd.Context(), "migration", m)
		cmd.SetContext(ctx)
	},
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func addSubCommandsPalettes() {
	MigrateCmd.AddCommand(UpCmd)
	MigrateCmd.AddCommand(DownCmd)
	MigrateCmd.AddCommand(ResetCmd)
}

func init() {
	// Here you will define your flags and configuration settings.

	MigrateCmd.PersistentFlags().IntP("steps", "s", 1, "Number of steps to migrate")
	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// migrateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// migrateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	addSubCommandsPalettes()
}
