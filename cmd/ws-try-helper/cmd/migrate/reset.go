/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package migrate

import (
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/spf13/cobra"
)

// resetCmd represents the reset command
var ResetCmd = &cobra.Command{
	Use:   "reset",
	Short: "Tear down all migration and migrate up all",
	Long:  `Tear down all migration and migrate up all`,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()

		m := ctx.Value("migration").(*migrate.Migrate)
		m.Down()
		m.Up()
		fmt.Println("reset migrations")
	},
}

func init() {
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// resetCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// resetCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
