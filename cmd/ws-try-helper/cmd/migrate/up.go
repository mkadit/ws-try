/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package migrate

import (
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/spf13/cobra"
)

// upCmd represents the up command
var UpCmd = &cobra.Command{
	Use:   "up",
	Short: "Migrate up",
	Long:  `Migrate up`,
	Run: func(cmd *cobra.Command, args []string) {
		steps, _ := cmd.Flags().GetInt("steps")
		ctx := cmd.Context()

		m := ctx.Value("migration").(*migrate.Migrate)
		m.Steps(steps)
		fmt.Printf("%d steps migrate up\n", steps)
	},
}

func init() {
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// upCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// upCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
