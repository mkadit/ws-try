/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"os"
	"gitlab.com/mkadit/ws-try/cmd/ws-try-helper/cmd/generate"
	"gitlab.com/mkadit/ws-try/cmd/ws-try-helper/cmd/migrate"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "ws-try-helper",
	Short: "Helper for ws-try",
	Long:  `Helper for ws-try`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func addSubCommandsPalettes() {
	rootCmd.AddCommand(generate.GenerateCmd)
	rootCmd.AddCommand(migrate.MigrateCmd)
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.ws-try-helper.yaml)")
	rootCmd.PersistentFlags().StringP("env", "e", "prod", "The environment of the application")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	addSubCommandsPalettes()
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.Set("DB_HOST", "localhost")
	viper.Set("DB_PORT", 5432)
	viper.Set("DB_USER", "suisei")
	viper.Set("DB_PASSWORD", "suisei")
	viper.Set("DB_NAME", "ws-try-db")
	viper.Set("REDIS_HOST", "localhost")
	viper.Set("REDIS_PORT", 6379)
	viper.Set("REDIS_PASSWORD", "SuiseiKawaii")
	viper.Set("REDIS_TTL", "2h")
	viper.Set("WS_TRY_BACKEND_HOST", "localhost")
	viper.Set("WS_TRY_BACKEND_PORT", 8001)
	viper.Set("SWAGGER_HOST", "http://localhost:8001")

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".ws-try-helper" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".ws-try-helper")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}
