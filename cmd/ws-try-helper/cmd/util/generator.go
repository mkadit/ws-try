package util

import (
	"context"
	"sync"
	"gitlab.com/mkadit/ws-try/internal/util"

	db "gitlab.com/mkadit/ws-try/internal/db/repository"
)

func GenerateUser(wg *sync.WaitGroup, queryDB *db.Queries, ctx context.Context, num int) {
	for i := 0; i < num; i++ {
		email := util.RandStringBytes(8) + "@" + util.RandStringBytes(5) + "." + util.RandStringBytes(3)
		queryDB.CreateUser(ctx, db.CreateUserParams{
			Email:    email,
			Username: util.RandStringBytes(10),
			Password: util.RandStringBytes(10),
		})
	}
}
