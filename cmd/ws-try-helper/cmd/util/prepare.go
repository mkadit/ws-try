package util

import (
	"database/sql"
	"errors"
	"gitlab.com/mkadit/ws-try/common/config"
	"gitlab.com/mkadit/ws-try/internal/api"
	"gitlab.com/mkadit/ws-try/internal/cache"
	"gitlab.com/mkadit/ws-try/internal/util"

	// great logs

	db "gitlab.com/mkadit/ws-try/internal/db/repository"

	"github.com/go-chi/chi/v5"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	// driver
	_ "github.com/lib/pq"
)

func ConnectStack() api.Server {
	// for generate data in localhost only, change accordingly
	conf, err := config.LoadConfig(config.ProjectRootPath)
	if err != nil {
		log.Fatal().Err(errors.New("cannot load env file")).Msgf("%s", err)
	}

	conn, err := sql.Open("postgres", conf.DBSource)
	if err != nil {
		log.Fatal().Err(errors.New("could not connect to database")).Msgf("%s", err)
	}

	util.MigrateDatabase(conn)

	database := db.New(conn)

	r := chi.NewRouter()

	redisClient := cache.NewRedisClient(conf)
	return api.MakeServer(conf, database, r, redisClient)
}

func ConnectMigration() (*migrate.Migrate, error) {
	// for generate data in localhost only, change accordingly
	conf, err := config.LoadConfig(config.ProjectRootPath)
	if err != nil {
		log.Fatal().Err(errors.New("cannot load env file")).Msgf("%s", err)
	}

	conn, err := sql.Open("postgres", conf.DBSource)
	if err != nil {
		log.Fatal().Err(errors.New("could not connect to database")).Msgf("%s", err)
	}

	driver, err := postgres.WithInstance(conn, &postgres.Config{})
	if err != nil {
		log.Fatal().Err(errors.New("unable to create db instance")).Msgf("%s", err)
	}
	migrationsLoc := "file://" + config.ProjectRootPath + "/internal/db/migrations/"
	m, err := migrate.NewWithDatabaseInstance(migrationsLoc, "postgres", driver)
	if err != nil {
		log.Fatal().Err(errors.New("unable to migrate db")).Msgf("%s", err)
	}

	return m, err
}

func CallPersistentPreRun(cmd *cobra.Command, args []string) {
	if parent := cmd.Parent(); parent != nil {
		if parent.PersistentPreRun != nil {
			parent.PersistentPreRun(parent, args)
		}
	}
}
