package main

import (
	"database/sql"

	"gitlab.com/mkadit/ws-try/common/config"
	"gitlab.com/mkadit/ws-try/internal/api"
	"gitlab.com/mkadit/ws-try/internal/cache"
	db "gitlab.com/mkadit/ws-try/internal/db/repository"
	"gitlab.com/mkadit/ws-try/internal/util"

	"github.com/go-chi/chi/v5"
)

func main() {
	// rand.Seed(time.Now().UTC().UnixNano())
	conf, err := config.LoadConfig(config.ProjectRootPath)
	if err != nil {
		util.LogFatal(err, "cannot load env file")
	}

	err = util.SetLogger(conf)
	if err != nil {
		util.LogFatal(err, "logger cannot be set")
	}

	util.LogInfoMsg("db: connecting to db")
	conn, err := sql.Open("postgres", conf.DBSource)
	if err != nil {
		util.LogFatal(err, "could not connect to database")
	}
	defer conn.Close()

	util.LogInfoMsg("db: migrating database")
	util.MigrateDatabase(conn)

	database := db.New(conn)

	util.LogInfoMsg("server: creating http server")
	r := chi.NewRouter()

	util.LogInfoMsg("cache: connecting to redis")
	redisClient := cache.NewRedisClient(conf)

	s := api.MakeServer(conf, database, r, redisClient)
	s.RunServer()
}
