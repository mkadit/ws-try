package config

import (
	"fmt"
	"path/filepath"
	"runtime"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/spf13/viper"

	_ "github.com/lib/pq"
)

var (
	_, b, _, _        = runtime.Caller(0)
	ProjectRootPath   = filepath.Join(filepath.Dir(b), "../..")
	TimeLoc, _        = time.LoadLocation(AreaLocation)
	JwtSignMethod     = jwt.SigningMethodHS512
	AppSecret         []byte
	JwtExpireDuration = time.Duration(2) * time.Hour
	Routes            []string
)

const (
	AppName      = "WS-TRY"
	TimeFormat   = "2006-01-02"
	AreaLocation = "Asia/Jakarta"
)

// Config stores all configuration of the application.
// The values are read by viper from a config file or environment variable.
type Config struct {
	SwaggerHost          string `mapstructure:"SWAGGER_HOST"`
	BillerAPIHost        string `mapstructure:"BILLER_API_HOST"`
	DBUser               string `mapstructure:"DB_USER"`
	DBPassword           string `mapstructure:"DB_PASSWORD"`
	DBName               string `mapstructure:"DB_NAME"`
	DBPort               string `mapstructure:"DB_PORT"`
	Environment          string `mapstructure:"ENV"`
	DBHost               string `mapstructure:"DB_HOST"`
	SitePort             string `mapstructure:"WS_TRY_BACKEND_PORT"`
	SiteHost             string `mapstructure:"WS_TRY_BACKEND_HOST"`
	ProdRoot             string `mapstructure:"PROD_ROOT"`
	TokenSymmetricKey    string `mapstructure:"TOKEN_SYMMETRIC_KEY"`
	RedisHost            string `mapstructure:"REDIS_HOST"`
	RedisPort            string `mapstructure:"REDIS_PORT"`
	RedisPassword        string `mapstructure:"REDIS_PASSWORD"`
	DBSource             string
	RedisTTL             time.Duration `mapstructure:"REDIS_TTL"`
	AccessTokenDuration  time.Duration `mapstructure:"ACCESS_TOKEN_DURATION"`
	RefreshTokenDuration time.Duration `mapstructure:"REFRESH_TOKEN_DURATION"`
}

// LoadConfig reads configuration from file or environment variables.
func LoadConfig(path string) (config Config, err error) {
	viper.AutomaticEnv()
	config.Environment = viper.Get("ENV").(string)
	if config.Environment == "prod" {
		config.SwaggerHost = viper.GetString("SWAGGER_HOST")
		config.DBUser = viper.GetString("DB_USER")
		config.DBPassword = viper.GetString("DB_PASSWORD")
		config.DBName = viper.GetString("DB_NAME")
		config.DBPort = viper.GetString("DB_PORT")
		config.RedisHost = viper.GetString("REDIS_HOST")
		config.RedisPort = viper.GetString("REDIS_PORT")
		config.RedisPassword = viper.GetString("REDIS_PASSWORD")
		config.RedisTTL = viper.GetDuration("REDIS_TTL")
		config.Environment = viper.GetString("ENV")
		config.DBHost = viper.GetString("DB_HOST")
		config.SitePort = viper.GetString("GO_URL_BACKEND_PORT")
		config.SiteHost = viper.GetString("GO_URL_BACKEND_HOST")
		config.ProdRoot = viper.GetString("PROD_ROOT")
		config.TokenSymmetricKey = viper.GetString("TOKEN_SYMMETRIC_KEY")
		config.BillerAPIHost = viper.GetString("BILLER_API_HOST")
		config.AccessTokenDuration = viper.GetDuration("ACCESS_TOKEN_DURATION")
		ProjectRootPath = config.ProdRoot

	} else {
		viper.AddConfigPath(path)

		// for Docker
		viper.SetConfigName("app")

		viper.SetConfigType("env")
		viper.AutomaticEnv()

		err = viper.ReadInConfig()
		if err != nil {
			return
		}

		err = viper.Unmarshal(&config)

		ProjectRootPath = filepath.Join(filepath.Dir(b), "../..")

	}
	if config.AccessTokenDuration != time.Duration(0) {
		JwtExpireDuration = config.AccessTokenDuration
	}
	config.DBSource = fmt.Sprintf("postgresql://%s:%s@%s:%s/%s?sslmode=disable", config.DBUser, config.DBPassword, config.DBHost, config.DBPort, config.DBName)
	return
}
