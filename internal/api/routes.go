package api

import (
	"fmt"
	"net/http"
	"gitlab.com/mkadit/ws-try/internal/user"
	"gitlab.com/mkadit/ws-try/internal/util"
	"gitlab.com/mkadit/ws-try/internal/ws"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/rs/zerolog/log"
)

// @title ws-try
// @version 1.0
// @description server for chat application
// @BasePath /
func (s *Server) SetupRouter() {
	userService := user.NewService(s.DB, s.Redis, s.Config)
	userHandler := user.NewHandler(userService)

	wsService := ws.NewService()
	hub := ws.NewHub()
	go hub.Run()
	wsHandler := ws.NewHandler(wsService, hub)

	apiRouter := chi.NewRouter()
	apiRouter.Use(util.LoggerMiddleware(&log.Logger))
	apiRouter.Use(middleware.Recoverer)
	apiRouter.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	apiRouter.Group(func(r chi.Router) {
		r.Get("/", userHandler.HTTPTesting)
	})

	wsRouter := chi.NewRouter()
	wsRouter.Get("/joinRoom/{roomId}", wsHandler.ConnectRoom)
	wsRouter.Post("/create", wsHandler.CreateRoom)

	userRoute := chi.NewRouter()

	userRoute.Get("/", userHandler.ListUser)
	userRoute.Group(func(r chi.Router) {
		r.Use(user.ValidateGetUserRequest)
		r.Get("/{user_id}", userHandler.GetUser)
	})

	userRoute.Group(func(r chi.Router) {
		r.Use(user.ValidateCreateUserRequest)
		r.Post("/", userHandler.CreateUser)
	})

	apiRouter.Mount("/user", userRoute)
	s.Router.Mount("/api", apiRouter)
	s.Router.Mount("/ws", wsRouter)

	chi.Walk(s.Router, func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		fmt.Printf("[%s]: '%s' has %d middlewares\n", method, route, len(middlewares))
		return nil
	})
}
