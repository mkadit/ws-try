package api

import (
	"fmt"
	"net/http"
	"gitlab.com/mkadit/ws-try/common/config"
	"gitlab.com/mkadit/ws-try/docs"
	"gitlab.com/mkadit/ws-try/internal/util"

	"github.com/go-chi/chi/v5"
	"github.com/redis/go-redis/v9"
	httpSwagger "github.com/swaggo/http-swagger"

	db "gitlab.com/mkadit/ws-try/internal/db/repository"
)

type Server struct {
	Router *chi.Mux
	DB     *db.Queries
	Redis  *redis.Client
	Config config.Config
}

// MakeServer Server creation
func MakeServer(conf config.Config, db *db.Queries, r *chi.Mux, redisClient *redis.Client) Server {
	server := Server{
		Router: r,
		DB:     db,
		Config: conf,
		Redis:  redisClient,
	}
	return server
}

// RunServer Run server
func (s *Server) RunServer() {
	s.SetupSwagger()
	s.SetupRouter()

	port := fmt.Sprintf(":%s", s.Config.SitePort)
	util.LogInfoMsg("listening to port: %s", port)

	err := http.ListenAndServe(port, s.Router)
	if err != nil {
		util.LogFatal(err, "failed to listen to port: %s", port)
	}
}

// SetupSwagger Setup swagger middleware
// Access in /docs/index.html
func (s *Server) SetupSwagger() {
	util.LogInfoMsg("docs: setting up swagger")
	docs.SwaggerInfo.Title = "Swagger API"
	docs.SwaggerInfo.Description = "Service to test websocket"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = s.Config.SwaggerHost
	docs.SwaggerInfo.Schemes = []string{"http"}

	s.Router.Get("/docs/*", httpSwagger.Handler(httpSwagger.URL("http://"+s.Config.SwaggerHost+"/docs/doc.json")))
}
