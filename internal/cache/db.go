package cache

import (
	"fmt"
	"gitlab.com/mkadit/ws-try/common/config"

	"github.com/redis/go-redis/v9"
)

// NewRedisClient Create new Redis Client
func NewRedisClient(conf config.Config) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", conf.RedisHost, conf.RedisPort),
		Password: conf.RedisPassword,
		DB:       0,
	})
}
