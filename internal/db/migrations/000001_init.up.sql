CREATE TABLE app_users(
	id_user SERIAL PRIMARY KEY,
  email VARCHAR(100) NOT NULL UNIQUE,
  username VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  created_at timestamptz NOT NULL DEFAULT (now()),
  last_login timestamptz
);
