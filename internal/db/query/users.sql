-- name: ListUsers :many
SELECT username, email, created_at FROM app_users LIMIT $1;

-- name: GetUser :one
SELECT username, email, created_at FROM app_users WHERE id_user = $1 LIMIT 1;

-- name: CreateUser :one
INSERT INTO app_users (
  email,
  username,
  password
) VALUES (
  $1, $2, $3
) RETURNING *;

-- name: ManyUsers :one
SELECT count(*) FROM app_users;
