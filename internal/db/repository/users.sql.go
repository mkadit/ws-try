// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.22.0
// source: users.sql

package db

import (
	"context"
	"time"
)

const createUser = `-- name: CreateUser :one
INSERT INTO app_users (
  email,
  username,
  password
) VALUES (
  $1, $2, $3
) RETURNING id_user, email, username, password, created_at, last_login
`

type CreateUserParams struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func (q *Queries) CreateUser(ctx context.Context, arg CreateUserParams) (AppUser, error) {
	row := q.db.QueryRowContext(ctx, createUser, arg.Email, arg.Username, arg.Password)
	var i AppUser
	err := row.Scan(
		&i.IDUser,
		&i.Email,
		&i.Username,
		&i.Password,
		&i.CreatedAt,
		&i.LastLogin,
	)
	return i, err
}

const getUser = `-- name: GetUser :one
SELECT username, email, created_at FROM app_users WHERE id_user = $1 LIMIT 1
`

type GetUserRow struct {
	Username  string    `json:"username"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
}

func (q *Queries) GetUser(ctx context.Context, idUser int32) (GetUserRow, error) {
	row := q.db.QueryRowContext(ctx, getUser, idUser)
	var i GetUserRow
	err := row.Scan(&i.Username, &i.Email, &i.CreatedAt)
	return i, err
}

const listUsers = `-- name: ListUsers :many
SELECT username, email, created_at FROM app_users LIMIT $1
`

type ListUsersRow struct {
	Username  string    `json:"username"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
}

func (q *Queries) ListUsers(ctx context.Context, limit int32) ([]ListUsersRow, error) {
	rows, err := q.db.QueryContext(ctx, listUsers, limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []ListUsersRow
	for rows.Next() {
		var i ListUsersRow
		if err := rows.Scan(&i.Username, &i.Email, &i.CreatedAt); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const manyUsers = `-- name: ManyUsers :one
SELECT count(*) FROM app_users
`

func (q *Queries) ManyUsers(ctx context.Context) (int64, error) {
	row := q.db.QueryRowContext(ctx, manyUsers)
	var count int64
	err := row.Scan(&count)
	return count, err
}
