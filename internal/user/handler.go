package user

import (
	"net/http"
	"strconv"
	"gitlab.com/mkadit/ws-try/internal/util"

	"github.com/go-chi/render"
)

type Handler struct {
	Service Service
}

// NewHandler Create handler for payment
func NewHandler(service Service) *Handler {
	return &Handler{
		Service: service,
	}
}

// @Summary      Endpoint to test access
// @Description  test endpoint
// @Tags         Test
// @Produce      json
// @Success      200  {object}  string
// @Failure      400  {object}  util.ErrorResponse
// @Failure      500  {object}  util.ErrorResponse
// @Router       /api [get]
func (h *Handler) HTTPTesting(w http.ResponseWriter, r *http.Request) {
	data := map[string]string{"message": "something"}
	render.JSON(w, r, data)
}

// @Summary      Endpoint to get user by id
// @Description  get user by id
// @Tags         users
// @Produce      json
// @Param        id   path      int true "User ID"
// @Success      200  {object}  GetUserResponse
// @Failure      400  {object}  util.ErrorResponse
// @Failure      500  {object}  util.ErrorResponse
// @Router       /api/user/{id} [get]
func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	getUserRequest := r.Context().Value("GetUserRequest").(*GetUserRequest)
	util.LogInfoInterface[GetUserRequest](getUserRequest, "Send request to server: %s", r.URL.Path)
	getUserResponse, status, err := h.Service.GetUser(r.Context(), *getUserRequest)
	util.LogInfoInterface[GetUserResponse](&getUserResponse, "Got Response from server: %s", r.URL.Path)
	if err != nil {
		render.Status(r, status)
		render.JSON(w, r, util.ErrorResponse{
			Message: err.Error(),
		})
		return
	}

	render.Status(r, status)
	render.JSON(w, r, getUserResponse)
}

// @Summary      Endpoint to get list of users
// @Description  list users
// @Tags         users
// @Produce      json
// @Param        many   query      int false "Amount of Returned"
// @Success      200  {object}  ListUserResponse
// @Failure      400  {object}  util.ErrorResponse
// @Failure      500  {object}  util.ErrorResponse
// @Router       /api/user [get]
func (h *Handler) ListUser(w http.ResponseWriter, r *http.Request) {
	many := r.URL.Query().Get("many")
	manyInt, ok := strconv.ParseInt(many, 10, 0)
	if ok != nil {
		manyInt = 10
	}

	listUserRequest := ListUserRequest{
		Many: int(manyInt),
	}
	util.LogInfoInterface[ListUserRequest](&listUserRequest, "Send Request to server: %s", r.URL.Path)
	listUserResponse, status, err := h.Service.ListUser(r.Context(), listUserRequest)
	util.LogInfoInterface[ListUserResponse](&listUserResponse, "Got Response to server: %s", r.URL.Path)
	if err != nil {
		render.Status(r, status)
		render.JSON(w, r, util.ErrorResponse{
			Message: err.Error(),
		})
		return
	}

	render.Status(r, status)
	render.JSON(w, r, listUserResponse)
}

// @Summary      Endpoint to create user
// @Description  create users
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        id   body      CreateUserRequest true "Body Param"
// @Success      200  {object}  CreateUserResponse
// @Failure      400  {object}  util.ErrorResponse
// @Failure      500  {object}  util.ErrorResponse
// @Router       /api/user [post]
func (h *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	createUserRequest := r.Context().Value("CreateUserRequest").(*CreateUserRequest)
	util.LogInfoInterface[CreateUserRequest](createUserRequest, "Send request to server: %s", r.URL.Path)
	getUserResponse, status, err := h.Service.CreateUser(r.Context(), *createUserRequest)
	util.LogInfoInterface[CreateUserResponse](&getUserResponse, "Got Response from server: %s", r.URL.Path)
	if err != nil {
		render.Status(r, status)
		render.JSON(w, r, util.ErrorResponse{
			Message: err.Error(),
		})
		return
	}

	render.Status(r, status)
	render.JSON(w, r, getUserResponse)
}
