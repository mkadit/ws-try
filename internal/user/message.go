package user

import db "gitlab.com/mkadit/ws-try/internal/db/repository"

type ErrorValidationMessage struct {
	Field string
	Tag   string
	Value string
}

type GetUserRequest struct {
	IdUser int `json:"username" validate:"required"`
}

type GetUserResponse struct {
	Username string `json:"username"`
	Email    string `json:"email"`
}

type ListUserRequest struct {
	Many int `json:"many"`
}

type ListUserResponse struct {
	Users []db.ListUsersRow `json:"users"`
}

type CreateUserRequest struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
	Email    string `json:"email" validate:"required"`
}

type CreateUserResponse struct {
	Username string `json:"username"`
}
