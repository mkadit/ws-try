package user

import (
	"context"
	"net/http"
	"gitlab.com/mkadit/ws-try/common/config"
	db "gitlab.com/mkadit/ws-try/internal/db/repository"
	"gitlab.com/mkadit/ws-try/internal/util"

	"github.com/redis/go-redis/v9"
)

type service struct {
	queryDB    *db.Queries
	queryCache *redis.Client
	conf       config.Config
}

// Service List of method for Inquiry
type Service interface {
	GetUser(context.Context, GetUserRequest) (GetUserResponse, int, error)
	ListUser(context.Context, ListUserRequest) (ListUserResponse, int, error)
	CreateUser(context.Context, CreateUserRequest) (CreateUserResponse, int, error)
}

// NewService Create service for payment
func NewService(db *db.Queries, redisClient *redis.Client, conf config.Config) *service {
	return &service{db, redisClient, conf}
}

// GetInquiryForm Get-Inquiry-Form service
func (s *service) GetUser(ctx context.Context, getUserRequest GetUserRequest) (GetUserResponse, int, error) {
	user, err := s.queryDB.GetUser(ctx, int32(getUserRequest.IdUser))
	if err != nil {
		util.LogError(err, "failed to obtain user by id")
		return GetUserResponse{}, 0, err
	}
	return GetUserResponse{
		Username: user.Username,
		Email:    user.Email,
	}, http.StatusOK, nil
}

func (s *service) ListUser(ctx context.Context, listUserRequest ListUserRequest) (ListUserResponse, int, error) {
	users, err := s.queryDB.ListUsers(ctx, int32(listUserRequest.Many))
	if err != nil {
		util.LogError(err, "failed to obtain users")
		return ListUserResponse{}, 0, err
	}
	return ListUserResponse{Users: users}, http.StatusOK, nil
}

func (s *service) CreateUser(ctx context.Context, createUserRequest CreateUserRequest) (CreateUserResponse, int, error) {
	user, err := s.queryDB.CreateUser(ctx, db.CreateUserParams{
		Username: createUserRequest.Username,
		Password: createUserRequest.Password,
		Email:    createUserRequest.Email,
	})
	if err != nil {
		util.LogError(err, "failed to create users")
		return CreateUserResponse{}, 0, err
	}
	return CreateUserResponse{Username: user.Username}, http.StatusOK, nil
}
