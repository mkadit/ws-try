package user

import (
	"context"
	"net/http"
	"strconv"
	"gitlab.com/mkadit/ws-try/internal/util"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

func ValidateGetUserRequest(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		idUser := chi.URLParam(r, "user_id")
		idUserInt, err := strconv.Atoi(idUser)
		if err != nil {
			util.LogError(err, "failed to convert id_user to int")
			render.Status(r, http.StatusBadRequest)
			render.JSON(w, r, err)
			return
		}

		getUserRequest := &GetUserRequest{
			IdUser: idUserInt,
		}
		err = util.Validator.Struct(getUserRequest)
		if err != nil {
			errs := util.TranslateValidateErrorMessage(err)
			util.LogError(err, "bad request")
			render.Status(r, http.StatusBadRequest)
			render.JSON(w, r, errs)
			return
		}

		ctx := context.WithValue(r.Context(), "GetUserRequest", getUserRequest)

		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func ValidateCreateUserRequest(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		req, err := util.ParsedJSONBody[CreateUserRequest](r)
		if err != nil {
			render.Status(r, http.StatusBadRequest)
			render.JSON(w, r, err)
			return
		}

		err = util.Validator.Struct(req)
		if err != nil {
			errs := util.TranslateValidateErrorMessage(err)
			util.LogError(err, "bad request")
			render.Status(r, http.StatusBadRequest)
			render.JSON(w, r, errs)
			return
		}
		ctx := context.WithValue(r.Context(), "CreateUserRequest", req)

		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
