package util

import (
	"fmt"
	"gitlab.com/mkadit/ws-try/common/config"

	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt"
)

type ErrorValidationMessage struct {
	Field string
	Tag   string
	Value string
}

type GoUrlClaims struct {
	jwt.StandardClaims
	Username string `json:"username"`
	Email    string `json:"email"`
	Id       int    `json:"user_id"`
}

var Validator = validator.New(validator.WithRequiredStructEnabled())

func ParseToken(tokenString string) (*jwt.Token, error) {
	return jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("signing method invalid")
		} else if method != config.JwtSignMethod {
			return nil, fmt.Errorf("signing method invalid")
		}

		return config.AppSecret, nil
	})
}

// translateErrorMessage Translate error message should validation failed
func TranslateValidateErrorMessage(err error) []*ErrorValidationMessage {
	var errs []*ErrorValidationMessage
	for _, err := range err.(validator.ValidationErrors) {
		var el ErrorValidationMessage
		el.Field = err.Field()
		el.Tag = err.Tag()
		el.Value = err.Param()
		errs = append(errs, &el)

	}
	return errs
}
