package util

import (
	"math/rand"
	"time"
	"gitlab.com/mkadit/ws-try/common/config"
	// "github.com/golang-jwt/jwt"
	// db "gitlab.com/mkadit/go-url/internal/db/repository"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func CurrentTime() string {
	return time.Now().In(config.TimeLoc).Format(config.TimeFormat)
}

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

// AuthFrontEndInquiry Generate JWT Token for AuthFrontEnd
func AuthFrontEndInquiry() (string, error) {
	// claims := GoUrlClaims{
	// 	StandardClaims: jwt.StandardClaims{
	// 		Issuer:    config.AppName,
	// 		ExpiresAt: time.Now().Add(config.JwtExpireDuration).Unix(),
	// 	},
	// }

	// token := jwt.NewWithClaims(config.JwtSignMethod, claims)
	// signedKey := []byte(paramConfig.FrontendSecretkey)
	// signedToken, err := token.SignedString(signedKey)
	// if err != nil {
	// 	return "", err
	// }

	// return signedToken, nil
	return "s", nil
}
