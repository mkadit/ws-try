package util

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
	"gitlab.com/mkadit/ws-try/common/config"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type logFields struct {
	Error      error
	Method     string
	Path       string
	TLS        string
	Protocol   int
	StatusCode int
	Latency    float64
}

func createLogger(files ...*os.File) (writers []io.Writer) {
	writers = []io.Writer{
		&zerolog.FilteredLevelWriter{
			Writer: zerolog.LevelWriterAdapter{Writer: files[0]},
			Level:  zerolog.InfoLevel,
		},
		&zerolog.FilteredLevelWriter{
			Writer: zerolog.LevelWriterAdapter{Writer: files[1]},
			Level:  zerolog.ErrorLevel,
		},
	}
	return writers
}

func (lf *logFields) MarshalZerologObject(e *zerolog.Event) {
	e.
		Str("method", lf.Method).
		Str("path", lf.Path).
		Int("protocol", lf.Protocol).
		Str("TLS", lf.TLS).
		Int("status_code", lf.StatusCode).
		Float64("latency", lf.Latency).
		Str("tag", "request")

	if lf.Error != nil {
		e.Err(lf.Error)
	}
}

// SetLogger Set logger
func SetLogger(conf config.Config) (err error) {
	logFolder := config.ProjectRootPath + "/logs"
	if _, err := os.Stat(logFolder); os.IsNotExist(err) {
		os.Mkdir(logFolder, 0777)
	}

	errorLogFile, _ := os.OpenFile(
		logFolder+"/log.error",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0666,
	)

	debugLogFile, _ := os.OpenFile(
		logFolder+"/log.info",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0666,
	)
	multWriters := createLogger(debugLogFile, errorLogFile)
	zerolog.TimeFieldFormat = time.RFC3339
	switch conf.Environment {
	case "prod":
	default:
		consoleWrite := zerolog.ConsoleWriter{Out: os.Stdout}
		multWriters = append(multWriters, consoleWrite)
	}

	zerolog.ErrorFieldName = "err"
	writers := zerolog.MultiLevelWriter(multWriters...)
	log.Logger = zerolog.New(writers).With().Logger()

	return
}

func LoggerMiddleware(logger *zerolog.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			log := logger.With().Logger()

			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			startTime := time.Now()
			tls := "https"
			if r.TLS == nil {
				tls = "http"
			}

			fields := &logFields{
				Method:   r.Method,
				Path:     r.URL.Path,
				Protocol: r.ProtoMajor,
				TLS:      tls,
			}
			defer func() {
				rec := recover()

				// Recover and record stack traces in case of a panic
				if rec != nil {
					err, ok := rec.(error)
					if !ok {
						err = fmt.Errorf("%v", rec)
					}

					fields.Error = err
					http.Error(ww, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}

				fields.StatusCode = ww.Status()
				fields.Latency = float64(time.Since(startTime).Seconds())

				switch {
				case rec != nil:
					log.Error().EmbedObject(fields).Msg("panic recover")
				case fields.StatusCode >= 500:
					log.Error().EmbedObject(fields).Msg("server error")
				case fields.StatusCode >= 400:
					log.Error().EmbedObject(fields).Msg("client error")
				case fields.StatusCode >= 300:
					log.Warn().EmbedObject(fields).Msg("redirect")
				case fields.StatusCode >= 200:
					log.Info().EmbedObject(fields).Msg("success")
				case fields.StatusCode >= 100:
					log.Info().EmbedObject(fields).Msg("informative")
				default:
					log.Warn().EmbedObject(fields).Msg("unknown status")
				}
			}()

			next.ServeHTTP(ww, r)
		}
		return http.HandlerFunc(fn)
	}
}

func LogInfoMsg(message string, attr ...any) {
	log.Info().Timestamp().Msgf(message, attr...)
}

func LogInfoInterface[T any](object *T, message string, attr ...any) {
	log.Info().Timestamp().Interface(fmt.Sprintf("%T", object), object).Msgf(message, attr...)
}

func LogError(err error, message string, attr ...any) {
	log.Err(fmt.Errorf(message, attr...)).Timestamp().Msgf("%s", err)
}

func LogFatal(err error, message string, attr ...any) {
	log.Fatal().Err(fmt.Errorf(message, attr...)).Timestamp().Msgf("%s", err)
}
