package util

import (
	"encoding/json"
	"io"
	"net/http"
)

type ErrorResponse struct {
	Message string `json:"message" example:"Expected something"`
}

func ParsedJSONBody[T any](r *http.Request) (*T, error) {
	var result T
	body, err := io.ReadAll(r.Body)
	if err != nil {
		LogError(err, "failed to read request body")
	}
	if err := json.Unmarshal(body, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
