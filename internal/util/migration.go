package util

import (
	"database/sql"
	"errors"
	"gitlab.com/mkadit/ws-try/common/config"

	// great logs
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/rs/zerolog/log"

	// driver
	_ "github.com/lib/pq"
)

// MigrateDatabase Migrate up database one time
func MigrateDatabase(conn *sql.DB) {
	driver, err := postgres.WithInstance(conn, &postgres.Config{})
	if err != nil {
		log.Fatal().Err(errors.New("unable to create db instance")).Msgf("%s", err)
	}
	migrationsLoc := "file://" + config.ProjectRootPath + "/internal/db/migrations/"
	m, err := migrate.NewWithDatabaseInstance(migrationsLoc, "postgres", driver)
	if err != nil {
		log.Fatal().Err(errors.New("unable to migrate db")).Msgf("%s", err)
	}
	m.Steps(1)
}
