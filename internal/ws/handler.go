package ws

import (
	"net/http"
	"gitlab.com/mkadit/ws-try/internal/util"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/gorilla/websocket"
)

type Handler struct {
	Service Service
	Hub     *Hub
}

// NewHandler Create handler for websocket
func NewHandler(service Service, hub *Hub) *Handler {
	return &Handler{
		Service: service,
		Hub:     hub,
	}
}

var wsUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// @Summary      Endpoint to create Room
// @Description  create Rooms
// @Tags         rooms
// @Produce      json
// @Accept       json
// @Param        id   body      CreateRoomReq true "Body Param"
// @Success      200  {object}  CreateRoomReq
// @Failure      400  {object}  util.ErrorResponse
// @Failure      500  {object}  util.ErrorResponse
// @Router       /ws/createRoom [post]
func (h *Handler) CreateRoom(w http.ResponseWriter, r *http.Request) {
	req, err := util.ParsedJSONBody[CreateRoomReq](r)
	util.LogInfoInterface[CreateRoomReq](req, "Sending request to %s", r.URL.Path)

	if err != nil {
		render.JSON(w, r, util.ErrorResponse{
			Message: err.Error(),
		})
	}

	h.Hub.Rooms[req.ID] = &Room{
		ID:      req.ID,
		Name:    req.Name,
		Clients: make(map[string]*Client),
	}
	// log.Info().Interface("CreateRoomReq", req).Msg("Sending request to createRoom")
	util.LogInfoInterface[Room](h.Hub.Rooms[req.ID], "Room Created")

	render.JSON(w, r, req)
}

func (h *Handler) ConnectRoom(w http.ResponseWriter, r *http.Request) {
	conn, err := wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		// log.Err(errors.New("failed to connect to websocket")).Msgf("%s", err)
		util.LogError(err, "failed to connect to websocket")
		render.JSON(w, r, util.ErrorResponse{
			Message: err.Error(),
		})
	}

	roomID := chi.URLParam(r, "roomId")
	clientID := r.URL.Query().Get("userId")
	username := r.URL.Query().Get("username")

	client := &Client{
		Conn:     conn,
		Message:  make(chan *Message, 10),
		ID:       clientID,
		RoomID:   roomID,
		Username: username,
	}

	message := &Message{
		Content:  "A new user has joined the room",
		RoomID:   roomID,
		Username: username,
	}

	h.Hub.Register <- client
	h.Hub.Broadcast <- message

	go client.writeMessage()
	client.readMessage(h.Hub)
}
