package ws

type CreateRoomReq struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type CreateRoomResp struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
