package ws

type service struct{}

// Service List of method for Inquiry
type Service interface{}

// NewService Create service for payment
func NewService() *service {
	return &service{}
}
